<?php include 'header.php' ?>


<header class="w-100 position-sticky mb-4">
    <div class="w-100 d-flex logo justify-content-center align-items-center">
        <img src="img/visualris_logo.png" alt="">
    </div>
    <div class="w-100 menu d-block">
        <div class="container">
            <div class="row">
                <div class="col-3 no-column text-center item-menu">
                    <a href="index.php" class="d-block w-100 h-100 d-flex text-center align-items-center justify-content-center" ><span>Inicio</span></a> 
                </div>
                <div class="col-3 no-column text-center item-menu ">
                    <a href="reserva-de-horas.php" class="d-block w-100 h-100 d-flex text-center align-items-center justify-content-center" ><span>Reserva de horas</span></a> 
                </div>
                <div class="col-3 no-column text-center item-menu ">
                    <a href="mis-horas.php" class="d-block w-100 h-100 d-flex text-center align-items-center justify-content-center" ><span>Mis horas</span></a> 
                </div>
                <div class="col-3 no-column text-center item-menu active">
                    <a href="#" class="d-block w-100 h-100 d-flex text-center align-items-center justify-content-center" ><span>Resultado de exámenes</span></a> 
                </div>
            </div>
        </div>
    </div>

</header>

<div class="container">

    <div class="row">
        <div class="col-12">
            <h1>Resultado de éxamenes</h1>
        </div>

        <div id="wrapper" class="col-12 p-5 border mb-5">

            <div class="row justify-content-center mt-5">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">

                    <form>

                        <div class="form-group text-left mb-5">

                            <h4 class="mb-3 font-600">R.U.T.</h4>

                            <input id="rut" type="text" class="form-control form-control-lg" placeholder="Ingrese su RUT" maxlength="12">
                            <small class="color-acero" >Ej: 15.962.195-2</small>
                        </div>

                        <div class="form-group text-left mb-5 pb-4">

                            <h4 class="mb-3 font-600">Número de Solicitud</h4>

                            <input type="number" class="form-control form-control-lg" placeholder="Ejemplo: 12345456567">
                            <small class="form-text color-acero text-left">El número de solicitud lo encontrarás en el mail "Reserva de hora" enviado a tu correo al momento de reservar la cita.</small>

                        </div>

                        <button type="submit" class="btn btn-cta text-white text-uppercase btn-lg d-block mx-auto">Buscar</button>

                    </form>

                </div>
            </div>


        </div>
        
        <div class="col-12 text-center firma">
            <p>VisualRis © 2019</p>
        </div>
        
    </div>


</div>


<?php include 'footer.php' ?>