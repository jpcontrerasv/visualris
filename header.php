<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->

        <link rel="stylesheet" href="css/bootstrap.css?v=<?php echo rand(0,1000);?>">      
        <link rel="stylesheet" href="css/bootstrap-combobox.css?v=<?php echo rand(0,1000);?>">      
        <link rel="stylesheet" href="css/classic.css?v=<?php echo rand(0,1000);?>">      
        <link rel="stylesheet" href="css/classic.date.css?v=<?php echo rand(0,1000);?>">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        
        <link rel="stylesheet" href="style.css">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">

        <title>VisualRis</title>
    </head>
    <body>