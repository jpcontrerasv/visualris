<?php include 'header.php' ?>


<header class="w-100 position-sticky mb-4">
    <div class="w-100 d-flex logo justify-content-center align-items-center">
        <img src="img/visualris_logo.png" alt="">
    </div>
    <div class="w-100 menu d-block">
        <div class="container">
            <div class="row">
                <div class="col-3 no-column text-center item-menu">
                    <a href="index.php" class="d-block w-100 h-100 d-flex text-center align-items-center justify-content-center" ><span>Inicio</span></a> 
                </div>
                <div class="col-3 no-column text-center active item-menu ">
                    <a href="#" class="d-block w-100 h-100 d-flex text-center align-items-center justify-content-center" ><span>Reserva de horas</span></a> 
                </div>
                <div class="col-3 no-column text-center item-menu ">
                    <a href="mis-horas.php" class="d-block w-100 h-100 d-flex text-center align-items-center justify-content-center" ><span>Mis horas</span></a> 
                </div>
                <div class="col-3 no-column text-center item-menu">
                    <a href="resultado-de-examenes.php" class="d-block w-100 h-100 d-flex text-center align-items-center justify-content-center" ><span>Resultado de exámenes</span></a> 
                </div>
            </div>
        </div>
    </div>

</header>

<div class="container">

    <div class="row">
        <div class="col-12">
            <h1>Reserva de horas</h1>
            <p>Ingresa tus datos y preferencias para reservar una hora.</p>
        </div>

        <div id="wrapper" class="col-12 p-5 border mb-5">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">Datos del paciente</li>
                    <li class="breadcrumb-item">Busca tu hora</li>
                    <li class="breadcrumb-item">Revisa y reserva</li>
                </ol>
            </nav>


            <div class="row justify-content-center mt-5">
                <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">

                    <form>

                        
                        <!--RUT-->
                        <div class="form-group text-left mb-5">

                            <h4 class="mb-3 font-600">¿Para quien es la hora?</h4>

                            <input id="rut" type="text" class="form-control form-control-lg" placeholder="Ingrese el RUT del paciente" maxlength="12">

                            <small class="color-acero">Ej: 15.962.195-2</small>
                        </div>

                        <!--PREVISIÓN-->
                        <div class="form-group text-left mb-5">

                            <h4 class="mb-3 font-600">¿Cuál es la previsión del paciente?</h4>

                            <select class="combobox form-control" name="horizontal" required="required" size="3">
                                <option value="" selected="selected">Escribe Aquí o selecciona de la lista</option>
                                <option value="FONASA">Fondo Nacional de Salud (FONASA)</option>
                                <option value="banmedica">Isapre Banmédica</option>
                                <option value="FONASA">Fondo Nacional de Salud (FONASA)</option>
                                <option value="banmedica">Isapre Banmédica</option>
                                <option value="FONASA">Fondo Nacional de Salud (FONASA)</option>
                                <option value="banmedica">Isapre Banmédica</option>

                            </select>


                        </div>

                        <!--ESPECIALIDAD-->
                        <div class="form-group text-left mb-5">

                            <h4 class="mb-3 font-600">Elige la especialidad</h4>

                            <select class="combobox form-control" name="horizontal" required="required" size="3">
                                <option value="" selected="selected">Escribe Aquí o selecciona de la lista</option>
                                <option value="anestesiologia">Anestesiología</option>
                                <option value="broncoadulto">Broncopulmonar adulto</option>
                                <option value="anestesiologia">Anestesiología</option>
                                <option value="broncoadulto">Broncopulmonar adulto</option>
                                <option value="anestesiologia">Anestesiología</option>
                                <option value="broncoadulto">Broncopulmonar adulto</option>
                                <option value="anestesiologia">Anestesiología</option>
                                <option value="broncoadulto">Broncopulmonar adulto</option>
                                <option value="anestesiologia">Anestesiología</option>
                                <option value="broncoadulto">Broncopulmonar adulto</option>
                                <option value="anestesiologia">Anestesiología</option>
                                <option value="broncoadulto">Broncopulmonar adulto</option>


                            </select>


                        </div>

                        <!--PRESTACIÓN-->
                        <div class="form-group text-left mb-5">

                            <h4 class="mb-3 font-600">Prestación</h4>

                            <select class="combobox form-control" name="horizontal" required="required" size="3">
                                <option value="" selected="selected">Escribe Aquí o selecciona de la lista</option>
                                <option value="consulta-broncopulmonar-adulto">Consulta Broncopulmonar Adulto</option>
                            </select>


                        </div>

                        <!--FECHA-->
                        <div class="form-group text-left mb-5">

                            <h4 class="mb-3 font-600">Fecha de preferencia</h4>

                            <input type="text" class="form-control form-control-lg datepicker" placeholder="Fecha más cercana">
                            <small class="color-acero">Campo Opcional</small>

                        </div>

                        <!--ELEGIR FECHA-->
                        <div class="form-group text-left mb-5 pb-4 border-bottom">

                            <h4 class="mb-3 font-600">Elige Fecha</h4>

                            <div class="container-fluid">
                                <div class="row align-items-center">
                                    <div class="col-2 text-right d-flex justify-content-center align-items-center">
                                        <a class="display-4" href="#">&laquo;</a>
                                        <small>Semana 28-3</small>
                                    </div>
                                    <div class="col-8 text-center">
                                        <div class="w-100 border-bottom p-2 mb-2" >
                                            <h3 class="mb-1">Febrero</h3>
                                            <h5 class="mb-2" >Semana 4 al 10</h5>
                                        </div>
                                        <div class="d-flex justify-content-around">
                                            <div class="dia-semana active">
                                                Lun 4
                                            </div>
                                            <div class="dia-semana">
                                                Mar 5
                                            </div>
                                            <div class="dia-semana">
                                                Mie 6
                                            </div>
                                            <div class="dia-semana">
                                                Jue 7
                                            </div>
                                            <div class="dia-semana">
                                                Vie 8
                                            </div>
                                            <div class="dia-semana">
                                                Sab 9
                                            </div>
                                            <div class="dia-semana">
                                                Dom 10
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-2 text-left d-flex justify-content-center align-items-center">
                                        <small class="d-block w-100" >Semana 11-17</small>
                                        <a class="display-4" href="#">&raquo;</a>
                                    </div>
                                </div>

                                <div class="row bg-azul-claro py-3 px-2 mb-2 align-items-center rounded">
                                    <div class="col-9 text-left">
                                        <h4 class="text-white m-0 p-0">Clínica De Santiago</h4>
                                        <p class="text-white m-0">
                                            <small>Avenida Departamental 45 | </small>
                                            <small>9:00 a 18:00 | </small>
                                            <small>600600900 | 2829287</small>
                                        </p>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 text-center mb-3">
                                        <span>Profesionales y horarios disponibles</span>
                                    </div>
                                    <div class="col-12 mb-4 pb-1">
                                        <div>
                                            <p class="mb-1" ><span class="font-600">Rodolfo Andrés, Valenzuela Cáceres</span> &nbsp;&nbsp;<small><a href="#" data-toggle="modal" data-target="#profesionalA">Ver detalle del profesional</a></small> </p>
                                        </div>

                                        <!-- Modal Profesional -->
                                        <div class="modal fade" id="profesionalA" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header bg-azul-claro">
                                                        <h5 class="modal-title text-white" id="exampleModalLabel">Detalle del profesional</h5>
                                                    </div>
                                                    <div class="modal-body">

                                                        <h6 class="mb-3 text-uppercase font-600" >Rodolfo Andrés, Valenzuela Cáceres</h6>

                                                        <h6 class="m-0 text-uppercase font-600" >Especialidades</h6>
                                                        <p>Medicina General</p>
                                                        <h6 class="m-0 text-uppercase font-600" >Atiende en</h6>
                                                        <p>Clínica de Santiago</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Close">Cerrar</button>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-around">
                                            <div class="dia-semana p-3 border active">
                                                09:00
                                            </div>
                                            <div class="dia-semana p-3 border">
                                                09:15
                                            </div>
                                            <div class="dia-semana p-3 border">
                                                09:30
                                            </div>
                                            <div class="dia-semana p-3 border">
                                                09:45
                                            </div>
                                            <div class="dia-semana p-3 border">
                                                10:00
                                            </div>
                                            <div class="dia-semana p-3 border">
                                                10:15
                                            </div>
                                            <div class="dia-semana p-3 border">
                                                10:30
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 mb-4 pb-1">
                                        <div>
                                            <p class="mb-1" ><span class="font-600">Rodolfo Andrés, Valenzuela Cáceres</span> &nbsp;&nbsp;<small><a href="#" data-toggle="modal" data-target="#profesionalB">Ver detalle del profesional</a></small> </p>
                                        </div>

                                        <!-- Modal Profesional -->
                                        <div class="modal fade" id="profesionalB" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header bg-azul-claro">
                                                        <h5 class="modal-title text-white" id="exampleModalLabel">Detalle del profesional</h5>
                                                    </div>
                                                    <div class="modal-body">

                                                        <h6 class="mb-3 text-uppercase font-600" >Rodolfo Andrés, Valenzuela Cáceres</h6>

                                                        <h6 class="m-0 text-uppercase font-600" >Especialidades</h6>
                                                        <p>Medicina General</p>
                                                        <h6 class="m-0 text-uppercase font-600" >Atiende en</h6>
                                                        <p>Clínica de Santiago</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Close">Cerrar</button>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="d-flex justify-content-around">
                                            <div class="dia-semana p-3 border active">
                                                09:00
                                            </div>
                                            <div class="dia-semana p-3 border">
                                                09:15
                                            </div>
                                            <div class="dia-semana p-3 border">
                                                09:30
                                            </div>
                                            <div class="dia-semana p-3 border">
                                                09:45
                                            </div>
                                            <div class="dia-semana p-3 border">
                                                10:00
                                            </div>
                                            <div class="dia-semana p-3 border">
                                                10:15
                                            </div>
                                            <div class="dia-semana p-3 border">
                                                10:30
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                        

                        <!--CORREO ELECTRÓNICO-->
                        <div class="form-group text-left mb-5">

                            <h4 class="mb-3 font-600">Ingresa tu correo electrónico</h4>

                            <input type="email" class="form-control form-control-lg" placeholder="Correo electrónico" maxlength="12" required>

                            <small class="color-acero">* Te enviaremos el comprobante de reserva a este mail</small>
                        </div>


                        <!--NUMERO DE CELULAR-->
                        <div class="form-group text-left mb-5">

                            <h4 class="mb-3 font-600">Ingresa tu número de celular</h4>

                            <input type="tel" class="form-control form-control-lg" placeholder="Celular" maxlength="12" required>

                            <small class="color-acero">Ej: 9 XXXX XXXX (9 dígitos)</small>
                        </div>
                        
                        


                        <!--VERIFICACIÓN RESERVA-->
                        <div class="form-group text-left mb-5">
                        <div class="row mb-4">
                            <div class="col-12 text-center">
                                <h4>Comprueba que todos los datos de tu reserva sean correctos antes de reservar.</h4>
                            </div>
                            <div class="col-4">
                                <h5 class="color-azul p-2 mb-2 border-bottom font-600 text-white bg-azul-claro">¿Quien reserva? </h5>
                                <p class="mb-2">Juan Pablo Contreras Valdés</p>
                                <p class="mb-2">15.962.195-2</p>
                                <p class="mb-2">Fondo Nacional De Salud (Fonasa)</p>
                            </div>
                            <div class="col-4">
                                <h5 class="color-azul p-2 mb-2 border-bottom font-600 text-white bg-azul-claro">¿Quien atiende? </h5>
                                <p class="mb-2">Dra. Nitza Mijal, Staub Rodio</p>
                                <p class="mb-2">Medicina General - Consulta Medicina general adulto</p>
                            </div>
                            <div class="col-4">
                                <h5 class="color-azul p-2 mb-2 border-bottom font-600 text-white bg-azul-claro">¿Dónde y cuándo? </h5>
                                <p class="mb-2">Clinica Bupa Santiago Av. Departamental 01455, La florida</p>
                                <p class="mb-2 font-600 color-azul" style="font-size:18px;">Jueves 14 de Febrero</p>
                                <p class="mb-2 font-600 color-azul" style="font-size:18px;">15:30Hrs</p>
                                <small>Recomendamos llegar con 10 minutos de anticipación.</small>
                            </div>
                            <div class="col-12 mt-4 text-center">
                                <small>Al reservar esta hora autorizo a Clínica Santiago a usar mis datos de contacto para enviarme recordatorios de mi reserva de esta hora.</small>
                            </div>

                        </div>
                        </div>
                        

                        <!--CONFIRMACIÓN RESERVA-->
                        <div class="form-group text-left mb-5 text-center">

                            <h1 class="mb-3 font-600 display-4">Reserva Agendada <i class="fas fa-check-circle"></i></h4>
                            <p>Enviaremos un mail a <span class="color-azul-claro"> mail@mail.com</span> con los datos de la reserva.</p>

                            <div class="rounded bg-azul-claro w-50 mx-auto p-3 d-flex justify-content-center align-items-center flex-column">
                                <h4 class="text-white font-600">Código de reserva</h4>
                                <span class="rounded p-2 color-azul bg-white display-4" >0082656998</span>
                            </div>

                            <small class="color-acero">Te enviaremos el código de reserva a tu mail</small>
                        </div>




                        

                        <button type="submit" class="btn btn-cta text-white text-uppercase btn-lg d-block mx-auto">Reservar</button>

                    </form>

                </div>
            </div>

        </div>

        <div class="col-12 text-center firma">
            <p>VisualRis © 2019</p>
        </div>

    </div>


</div>


<?php include 'footer.php' ?>