//PICKADATE
$('.datepicker').pickadate({
    monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
    today: 'hoy',
    clear: 'Limpiar',
    formatSubmit: 'dd/mm/yyyy',
    close: 'Cerrar',
     min: -1,
    max: false,
    firstDay: 2
});

$('.datepicker').prop('readonly', false);

//RUT
$("input#rut").rut();

//COMBOBOX
$(document).ready(function(){
    $('.combobox').combobox({
        renderLimit: 3
    })
});

//TOGGLE DIA SEMANA
$('.dia-semana').click(function() {
     $(this).toggleClass('active');
     $(this).siblings().removeClass('active');
   });

//TOOLTIP
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});

