<?php include 'header.php' ?>


<header class="w-100 position-sticky mb-4">
    <div class="w-100 d-flex logo justify-content-center align-items-center">
        <img src="img/visualris_logo.png" alt="">
    </div>
    <div class="w-100 menu d-block">
        <div class="container">
            <div class="row">
                <div class="col-3 no-column text-center item-menu">
                    <a href="index.php" class="d-block w-100 h-100 d-flex text-center align-items-center justify-content-center" ><span>Inicio</span></a> 
                </div>
                <div class="col-3 no-column text-center item-menu ">
                    <a href="reserva-de-horas.php" class="d-block w-100 h-100 d-flex text-center align-items-center justify-content-center" ><span>Reserva de horas</span></a> 
                </div>
                <div class="col-3 no-column text-center active item-menu ">
                    <a href="#" class="d-block w-100 h-100 d-flex text-center align-items-center justify-content-center" ><span>Mis horas</span></a> 
                </div>
                <div class="col-3 no-column text-center item-menu">
                    <a href="resultado-de-examenes.php" class="d-block w-100 h-100 d-flex text-center align-items-center justify-content-center" ><span>Resultado de exámenes</span></a> 
                </div>
            </div>
        </div>
    </div>

</header>

<div class="container">

    <div class="row">
        <div class="col-12">
            <h1>Mis horas</h1>
        </div>

        <div id="wrapper" class="col-12 p-5 border">

            <div class="row justify-content-center">
                <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">

                    <p>Para consultar, confirmar o anular tus horas reservadas, ingresa tu RUT y número de solicitud de la reserva. El número de solicitud lo encontrarás en tu correo enviado por "Reserva de Hora" cuando realizaste la reserva.</p>

                    <form>


                        <!--RUT-->
                        <div class="form-group text-left mb-5">

                            <h4 class="mb-3 font-600">RUT</h4>

                            <input id="rut" type="text" class="form-control form-control-lg" placeholder="Ingrese el RUT del paciente" maxlength="12">

                            <small class="color-acero">Ej: 15.962.195-2</small>
                        </div>


                        <!--RUT-->
                        <div class="form-group text-left mb-5">

                            <h4 class="mb-3 font-600">Número de solicitud</h4>

                            <input type="text" class="form-control form-control-lg" placeholder="Ingresa el número de reserva" maxlength="12">

                            <small class="color-acero">Encuentra tu número de solicitud en el mail de confirmación de reserva. Ej: 059597528</small>
                        </div>


                        <!--VERIFICACIÓN RESERVA-->
                        <div class="form-group text-left mb-5">
                            <div class="row mb-4">
                                <div class="col-12">
                                    <h1 class="mb-0" >Mis horas reservadas</h1>
                                    <p>Confirma o anula tus reservas</p>
                                </div>

                                <div class="col-12 mb-4">
                                    <div class="rounded bg-azul-claro w-50 mx-auto p-3 d-flex justify-content-center align-items-center flex-column">
                                        <h4 class="text-white font-600">Código de reserva</h4>
                                        <span class="rounded p-2 color-azul bg-white display-4" >0082656998</span>
                                    </div>
                                </div>

                                <div class="col-4">
                                    <h5 class="color-azul p-2 mb-2 border-bottom font-600 text-white bg-azul-claro">¿Quien reserva? </h5>
                                    <p class="mb-2">Juan Pablo Contreras Valdés</p>
                                    <p class="mb-2">15.962.195-2</p>
                                    <p class="mb-2">Fondo Nacional De Salud (Fonasa)</p>
                                </div>
                                <div class="col-4">
                                    <h5 class="color-azul p-2 mb-2 border-bottom font-600 text-white bg-azul-claro">¿Quien atiende? </h5>
                                    <p class="mb-2">Dra. Nitza Mijal, Staub Rodio</p>
                                    <p class="mb-2">Medicina General - Consulta Medicina general adulto</p>
                                </div>
                                <div class="col-4">
                                    <h5 class="color-azul p-2 mb-2 border-bottom font-600 text-white bg-azul-claro">¿Dónde y cuándo? </h5>
                                    <p class="mb-2">Clinica Bupa Santiago Av. Departamental 01455, La florida</p>
                                    <p class="mb-2 font-600 color-azul" style="font-size:18px;">Jueves 14 de Febrero</p>
                                    <p class="mb-2 font-600 color-azul" style="font-size:18px;">15:30Hrs</p>
                                    <small>Recomendamos llegar con 10 minutos de anticipación.</small>
                                </div>
                            </div>
                        </div>


                        <!--CONFIRMACIÓN RESERVA-->

                        <div class="col-12 text-center">

                            <button type="submit" class="btn btn-cta bg-white color-azul-claro border text-uppercase btn-lg d-inline-block mx-3">Anular</button>

                            <button type="submit" class="btn btn-cta text-white text-uppercase btn-lg d-inline mx-3">Confirmar</button>
                        </div>



                    </form>

                </div>
            </div>

        </div>

        <div class="col-12 text-center firma">
            <p>VisualRis © 2019</p>
        </div>

    </div>


</div>


<?php include 'footer.php' ?>